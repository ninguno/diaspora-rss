// diaspora-rss Bot - 2018

'use strict'

const Diaspora = require('node-diaspora'),
  Parser = require('rss-parser'),
  config = require('./config.json'),
  diasp = new Diaspora({
    user: config.user,
    password: config.pass,
    pod: config.pod
  }),
  parser = new Parser(),
  hours = config.hours

diasp.connect((err, suc) => {
  if(err) console.log(err)
  else {
    let oldfeed = {}
    oldfeed.items = []
    setInterval(() => {
      parser.parseURL(config.url, (err, feed) => {
        if(err) console.log(err)
        else if(feed && feed.items) {
          if(oldfeed.items.length === 0) {
            oldfeed = feed
          }
          else if(feed.items.length > 0) {
            feed.items.map(item => {
              let exists = oldfeed.items.some(olditem => {
                return item[config.property] === olditem[config.property]
              })
              if(!exists) {
                diasp.postStatusMessage(`${item.title} - ${item.link} - ${item.contentSnippet} by ${item.creator}`, 'public', (err, res) => {
                  if(err) console.log(err)
                  else console.log(res)
                })
              }
            })
            oldfeed = feed
          }
        }
      })
    }, hours * 3600000)
  }
})
